export * from './media'
export const colors = {
    white:'#fff',
    black:'#1e1e1e',
    primary:'#84C69C',
    secondary:'#EAF8EB',
    grey:'grey'
}