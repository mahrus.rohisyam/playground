import GOT_Logo from './goit.png';
import Food_Banner from './food-banner.jpg';
import Complain_Icon from './complaint.png';
import Security_Icon from './security.png';
import Billing_Icon from './billing.png';
import Account_Icon from './account.png';
import Chat_Icon from './chat.png';
import SOS_Icon from './sos.png';
import Home_Icon from './home.png'
import Account_Icon_Active from './accountActive.png';
import Chat_Icon_Active from './chatActive.png';
import SOS_Icon_Active from './sosActive.png';
import Home_Icon_Active from './homeActive.png'

export {
  GOT_Logo,
  Food_Banner,
  Complain_Icon,
  Security_Icon,
  Billing_Icon,
  Account_Icon,
  Chat_Icon,
  SOS_Icon,
  Home_Icon,
  Account_Icon_Active,
  Chat_Icon_Active,
  SOS_Icon_Active,
  Home_Icon_Active
};
