import React, {Component} from 'react';
import {StyleSheet, View, StatusBar, ScrollView} from 'react-native';
import {SafeAreaView} from 'react-native-safe-area-context';
import {colors} from '../../assets';

export default class Layout extends Component {
  render() {
    const {style, scroll} = this.props;

    return (
      <>
        <SafeAreaView style={{...style, ...styles.mainContainer}}>
        <StatusBar backgroundColor="#f2f2f2" barStyle="dark-content" />
          {scroll == 'yes' ? (
            <ScrollView>{this.props.children}</ScrollView>
          ) : (
            <View>{this.props.children}</View>
          )}
        </SafeAreaView>
      </>
    );
  }
}

const styles = StyleSheet.create({
  mainContainer: {
    flex: 1,
    // backgroundColor:colors.white
  },
});
