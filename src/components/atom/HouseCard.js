import React, {Component} from 'react';
import {Image, StyleSheet, View} from 'react-native';
import {CText} from '.';
import {colors} from '../../assets';
import MI from 'react-native-vector-icons/MaterialIcons';
import {convertDate, convertDateOnly} from '../../uttils';

export default class HouseCard extends Component {
  render() {
    const {featured, price, title, size, location, timestamps, photo_url} =
      this.props;

    return (
      <View style={styles.mainContainer}>
        {/* Left */}
        <View>
          <Image
            source={{uri: photo_url}}
            style={styles.thumbnail}
            resizeMode="cover"
          />
        </View>
        {/* Right */}

        <View style={{width: '72.5%', paddingHorizontal: 10}}>
          {featured && (
            <View style={styles.featured}>
              <CText>Featured</CText>
            </View>
          )}
          <CText style={{fontWeight: 'bold', fontSize: 18}}>Rp. {price}</CText>
          <CText>
            {title.length >= 28
              ? title.substring(0, 28).toUpperCase()
              : title.toUpperCase()}
            {title.length >= 28 && '...'}
          </CText>
          <CText>{size}</CText>
          <View style={styles.miscWrapper}>
            <View
              style={{flexDirection: 'row', justifyContent: 'space-between'}}>
              <MI name="location-on" size={17} />
              <CText style={styles.miscText}>
                {location.length >= 17
                  ? location.substring(0, 17).toUpperCase()
                  : location.toUpperCase()}
                {location.length >= 20 && '...'}
              </CText>
            </View>
            <CText style={styles.miscText}>
              {convertDateOnly(new Date(timestamps))}
            </CText>
          </View>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  mainContainer: {
    flexDirection: 'row',
    padding: 10,
    paddingVertical: 15,
    borderWidth: 1,
    borderColor: colors.grey,
    borderRadius: 10,
    elevation: 3,
    backgroundColor: colors.white,
  },
  thumbnail: {
    width: 125,
    height: 125,
    borderRadius: 10,
  },
  featured: {
    backgroundColor: colors.primary,
    padding: 2,
    width: 100,
    alignItems: 'center',
  },
  miscText: {
    fontSize: 12,
  },
  miscWrapper: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginTop: 20,
  },
});
