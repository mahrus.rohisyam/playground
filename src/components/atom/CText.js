import React, {Component} from 'react';
import {Text, StyleSheet, View} from 'react-native';

export default class CText extends Component {
  render() {
    const {style, children, bold} = this.props;
    return (
      <View>
        <Text style={{...styles.mainStyle, ...style}}>{children}</Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({});
