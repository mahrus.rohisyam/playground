import React, {Component} from 'react';
import {StyleSheet, View} from 'react-native';
import {CText} from '.';
import {colors} from '../../assets';

export default class Banner extends Component {
  render() {
    const {due_date, current_bill, status} = this.props;

    return (
      <View style={styles.mainContainer}>
        <View style={styles.twoColumn}>
          {/* Left */}
          <View>
            <CText style={{...styles.large, ...styles.white, ...styles.bold}}>
              IPL INVOICE
            </CText>
            <CText style={{...styles.white, ...{marginVertical: 10}}}>
              Status
            </CText>
          </View>
          {/* Right */}
          <View>
            <CText style={{...styles.large, ...styles.white, ...styles.bold}}>
              Rp. {current_bill}
            </CText>
            <View style={status == 'paid' ? styles.paid : styles.unpaid}>
              <CText
                style={
                  status == 'unpaid' && {
                    color: colors.white,
                    fontWeight: 'bold',
                  }
                }>
                {status == 'paid' ? 'Paid' : 'Unpaid'}
              </CText>
            </View>
          </View>
          {/* Bottom */}
        </View>
        <View style={styles.secondaryContainer}>
          <CText style={{color: colors.primary, ...styles.bold}}>
            Due date
          </CText>
          <CText style={{color: colors.primary, ...styles.bold}}>
            {due_date}
          </CText>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  mainContainer: {
    margin: 20,
    backgroundColor: colors.primary,
    borderRadius: 10,
    elevation: 5,
  },
  twoColumn: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingVertical: 15,
    paddingHorizontal: 25,
  },
  secondaryContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    backgroundColor: colors.secondary,
    paddingHorizontal: 25,
    paddingVertical: 10,
    borderBottomEndRadius: 10,
    borderBottomStartRadius: 10,
  },
  large: {
    fontSize: 20,
  },
  white: {
    color: colors.white,
  },
  bold: {
    fontWeight: 'bold',
  },
  paid: {
    backgroundColor: colors.secondary,
    padding: 5,
    borderRadius: 10,
    justifyContent: 'center',
    alignItems: 'center',
    marginVertical: 10,
  },
  unpaid: {
    backgroundColor: 'red',
    padding: 5,
    borderRadius: 10,
    justifyContent: 'center',
    alignItems: 'center',
    marginVertical: 10,
  },
});
