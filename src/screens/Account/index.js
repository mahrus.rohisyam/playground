import React, {Component} from 'react';
import {Text, StyleSheet, View, Image, TouchableOpacity} from 'react-native';
import {colors} from '../../assets';
import {CText} from '../../components';
import {userData} from '../../uttils';
import MCI from 'react-native-vector-icons/MaterialCommunityIcons';
import MI from 'react-native-vector-icons/MaterialIcons';
export default class index extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: userData,
      menuList: [
        {name: 'Settings', action: 'Nothing', Icon: 'settings'},
        {name: 'Billing Details', action: 'Nothing', Icon: 'card-text'},
        {name: 'User Management', action: 'Nothing', Icon: 'person'},
        {name: 'Information', action: 'Nothing', Icon: 'info'},
        {name: 'Logout', action: 'Nothing', Icon: 'power-settings-new'},
      ],
    };
  }

  render() {
    const {data, menuList} = this.state;

    return (
      <View style={{flex: 1}}>
        {/* Header Start */}
        <View style={styles.headerWrapper}>
          <Image
            resizeMode="cover"
            style={styles.profilePhoto}
            source={{uri: data.profile_photo}}
          />
          <CText style={styles.title}>{data.name}</CText>
          <CText style={styles.subTitle}>@{data.username}</CText>
        </View>
        {/* Header End */}

        {/* Body Start */}
        <View style={styles.menuWrapper}>
          {menuList.map((v, i) => {
            return (
              <TouchableOpacity style={styles.menuButton} key={i}>
                {v.Icon == 'card-text' ? (
                  <View style={styles.iconWrap}>
                    <MCI name={v.Icon} size={30} />
                  </View>
                ) : (
                  <View style={styles.iconWrap}>
                    <MI name={v.Icon} size={30} />
                  </View>
                )}
                <CText>{v.name}</CText>
                {/* Icon */}
              </TouchableOpacity>
            );
          })}
        </View>
        {/* Body End */}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  headerWrapper: {
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 50,
    marginBottom: 100,
  },
  profilePhoto: {
    width: 100,
    height: 100,
    borderRadius: 50,
  },
  menuWrapper: {
    backgroundColor: colors.white,
    padding: 20,
    borderTopLeftRadius: 20,
    borderTopEndRadius: 20,
    elevation: 10,
    flex: 1,
  },
  menuButton: {
    padding: 10,
    flexDirection:'row',
    alignItems:'center'
  },
  iconWrap:{
    justifyContent:'center',
    alignItems:'center',
    padding:5,
    backgroundColor:colors.grey,
    borderRadius:10,
    marginRight:10
  },
  title:{
    fontSize:20,
    fontWeight:'bold',
    marginTop:10
  }
});
