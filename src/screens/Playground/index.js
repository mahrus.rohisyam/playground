import React, {Component} from 'react';
import {Text, StyleSheet, View, Button} from 'react-native';

export default class index extends Component {
  constructor(params) {
    super(params);
    this.state = {
      button: [
        {name: 'Chat', route: 'Chat'},
        {name: 'Home', route: 'Home'},
        {name: 'Discover', route: 'Discover'},
        {name: 'Account', route: 'Account'},
      ],
    };
  }

  render() {
    const {button} = this.state;
    const {navigation} = this.props;

    return (
      <View>
        <Text> Test Navigasi </Text>
        {button.map((val, index) => {
          return (
            <View style={{margin:10}} key={index}>
              <Button
                title={val.name}
                onPress={() => {
                  navigation.replace(val.name);
                }}
              />
            </View>
          );
        })}
      </View>
    );
  }
}

const styles = StyleSheet.create({});
