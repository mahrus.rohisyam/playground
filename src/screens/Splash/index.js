import React, {Component} from 'react';
import {Text, StyleSheet, View, Image} from 'react-native';
import {GOT_Logo} from '../../assets';
import { CText } from '../../components';

export default class index extends Component {
  render() {
    const {navigation} = this.props;
    setTimeout(() => {
      navigation.replace('MainApp');
    }, 3000);

    return (
      <View style={styles.mainContainer}>
        <Image style={styles.logo} source={GOT_Logo} />
        <View style={styles.titleWrapper}>
          <CText style={styles.title}>GOIT.ID</CText>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  mainContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  logo: {
    width: 140,
    height: 100,
  },
  titleWrapper: {
    margin: 20,
  },
  title: {
    fontSize: 30,
    fontWeight: 'bold',
  },
});
