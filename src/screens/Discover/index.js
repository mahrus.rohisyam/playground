// import React, {Component} from 'react';
// import {
//   StyleSheet,
//   View,
//   TextInput,
//   ScrollView,
//   TouchableOpacity,
// } from 'react-native';
// import {CText, HouseCard, Layout} from '../../components';
// import MCI from 'react-native-vector-icons/MaterialCommunityIcons';
// import MI from 'react-native-vector-icons/MaterialIcons';
// import {colors} from '../../assets';
// import axios from 'axios';
// import {connect} from 'react-redux';

// export class index extends Component {
//   constructor(params) {
//     super(params);
//     this.state = {
//       slideMenu: [
//         {name: 'Property', action: 'Nothing', icon: 'office-building'},
//         {name: 'Car', action: 'Nothing', icon: 'car'},
//         {name: 'Industry', action: 'Nothing', icon: 'factory'},
//       ],
//       data: null,
//     };
//   }

//   async componentDidMount() {
//     axios
//       .get('https://api.jsonbin.io/b/61caef35c912fe67c50a59b7', {
//         headers: {
//           'secret-key':
//             '$2b$10$lK3WbPKRnedkTyREYfDP7uf7klpopOOSjJRQuiO8ksnUIcqiDLyEa',
//         },
//       })
//       .then(res => {
//         this.setState({
//           data: res.data.data,
//         });
//       });
//   }

//   store = async () => {};

//   render() {
//     const {slideMenu, data} = this.state;

//     return (
//       <Layout scroll="yes">
//         {/* Header Start */}
//         <View style={{margin: 15}}>
//           <View style={styles.header}>
//             <MI name="search" size={25} />
//             <TextInput placeholder="Find Something Here" />
//           </View>
//           {/* Header End */}

//           {/* Body Start */}
//           <View style={styles.slideMenu}>
//             <View
//               style={{
//                 flexDirection: 'row',
//                 justifyContent: 'space-between',
//                 marginVertical: 10,
//               }}>
//               <CText>Browse by Categories</CText>
//               <CText>See all</CText>
//             </View>
//             <ScrollView scrollEventThrottle={16} horizontal>
//               {slideMenu.map((v, i) => {
//                 return (
//                   <View key={i} style={{alignItems: 'center'}}>
//                     <TouchableOpacity style={styles.slideMenuButton}>
//                       <MCI name={v.icon} size={45} />
//                     </TouchableOpacity>
//                     <CText>{v.name}</CText>
//                   </View>
//                 );
//               })}
//             </ScrollView>
//           </View>
//         </View>

//         <ScrollView>
//           <View style={styles.bodyWrapper}>
//             <CText>Recomendations</CText>
//             <View style={{marginVertical: 20}}>
//               {data &&
//                 data.map((v, i) => {
//                   return (
//                     <View key={i} style={{marginVertical: 5}}>
//                       <HouseCard
//                         featured={i <= 1}
//                         price={v.price}
//                         title={v.title}
//                         size={v.size}
//                         location={v.location}
//                         timestamps={v.timestamp}
//                         photo_url={v.photo_uri}
//                       />
//                     </View>
//                   );
//                 })}
//             </View>
//           </View>
//         </ScrollView>
//         {/* Body End */}
//       </Layout>
//     );
//   }
// }

// const mapDispatchToProps = send => {
//   return {
//     update: data =>
//       send({
//         type: 'HOUSE-DATA',
//         payload: data,
//       }),
//   };
// };

// const mapStateToProps = state => {
//   return {
//     houseData: state.houseData,
//   };
// };

// export default connect(mapDispatchToProps, mapStateToProps)(index);

// const styles = StyleSheet.create({
//   header: {
//     borderWidth: 1,
//     flexDirection: 'row',
//     alignItems: 'center',
//     paddingHorizontal: 10,
//     borderRadius: 5,
//   },
//   slideMenu: {
//     padding: 10,
//   },
//   slideMenuButton: {
//     alignItems: 'center',
//     marginHorizontal: 10,
//     backgroundColor: colors.white,
//     padding: 5,
//     width: 80,
//     height: 80,
//     borderRadius: 15,
//     elevation: 5,
//     marginVertical: 2,
//     justifyContent: 'center',
//   },
//   bodyWrapper: {
//     paddingVertical: 20,
//     backgroundColor: colors.white,
//     borderTopStartRadius: 20,
//     borderTopEndRadius: 20,
//     paddingHorizontal: 15,
//     height: '100%',
//   },
// });

import React, {Component} from 'react';
import {Text, StyleSheet, View} from 'react-native';

export default class index extends Component {
  constructor() {
    super();
    this.state = {
      data: [],
    };
  }

  componentDidMount() {
    fetch('http://www.omdbapi.com/?s=avengers&apikey=997061b4')
      .then(res => res.json())
      .then(json => console.log(json));
  }

  render() {
    const {data} = this.state;
    console.log(data);

    return (
      <View>
        {/* {data && data.map((value, index) => {
          return <Text>{value.Title}</Text>
        })} */}
      </View>
    );
  }
}

const styles = StyleSheet.create({});
