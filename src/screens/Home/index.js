import React, {Component} from 'react';
import {Text, StyleSheet, View, Image, TouchableOpacity} from 'react-native';
import {
  Billing_Icon,
  colors,
  Complain_Icon,
  Food_Banner,
  Security_Icon,
} from '../../assets';
import {Banner, CText, Layout} from '../../components';
import {userData} from '../../uttils';
import {connect} from 'react-redux';

export class index extends Component {
  constructor(props) {
    super(props);
    this.state = {
      menuList: [
        {name: 'Status Billing', action: 'Nothing', icon: Billing_Icon},
        {name: 'Complaint', action: 'Nothing', icon: Complain_Icon},
        {name: 'Security', action: 'Nothing', icon: Security_Icon},
      ],
    };
  }

  render() {
    const {menuList} = this.state;
    const {userLogin} = this.props
    console.log(this.props.userLogin)

    return (
      <Layout>
        {/* Header Start */}
        <Banner
          status={userData.status}
          current_bill={userData.current_bill}
          due_date={userData.due_date}
        />
        <View style={styles.imageWrapper}>
          <Image
            source={Food_Banner}
            resizeMode="cover"
            style={styles.bannerImage}
          />
        </View>
        {/* Header End */}

        {/* Body Start */}
        <View style={styles.bodyWrapper}>
          <Text style={{fontWeight: 'bold'}}>Your Needs</Text>
          <View style={styles.menuWrapper}>
            {menuList.map((v, i) => {
              return (
                <View
                  style={{
                    marginHorizontal: 10,
                    maxWidth: 100,
                    alignItems: 'center',
                  }}
                  key={i}>
                  <TouchableOpacity
                    onPress={() => {
                      console.log('Test');
                    }}
                    style={styles.menuButton}>
                    <Image source={v.icon} style={styles.icon} />
                  </TouchableOpacity>
                  <CText>{v.name}</CText>
                </View>
              );
            })}
          </View>
        </View>
        {/* Body End */}
      </Layout>
    );
  }
} 

const mapStateToProps = state => {
  return {
    userLogin: state.userLogin,
    houseData: state.houseData
  };
};

export default connect(mapStateToProps)(index);

const styles = StyleSheet.create({
  bannerImage: {
    width: '100%',
    height: 150,
    borderRadius: 10,
  },
  imageWrapper: {
    marginHorizontal: 20,
  },
  bodyWrapper: {
    margin: 20,
  },
  menuWrapper: {
    flexDirection: 'row',
    flexWrap: 'wrap',
    marginTop: 10,
  },
  menuButton: {
    padding: 5,
    backgroundColor: colors.white,
    borderRadius: 10,
    elevation: 2,
  },
  icon: {
    height: 60,
    width: 60,
    margin: 10,
  },
});
