import React, {Component} from 'react';
import {Text, StyleSheet, View, TouchableOpacity} from 'react-native';
import {CText, Layout} from '../../components';
import MI from 'react-native-vector-icons/MaterialIcons';
import {colors} from '../../assets';
export default class index extends Component {
  constructor(params) {
    super(params);
    this.state = {
      faq_list: [
        {title: 'Tentang Program Kita', action: 'Nothing'},
        {title: 'Tentang Program Kita', action: 'Nothing'},
        {title: 'Tentang Program Kita', action: 'Nothing'},
      ],
    };
  }

  render() {
    const {faq_list} = this.state;
    return (
      <Layout>
        {/* Body Start */}
        <View style={styles.box}>
          <View style={styles.rowHeader}>
            <MI name="question-answer" size={18} />
            <CText style={{marginHorizontal: 10}}>Live Chat</CText>
          </View>
          <CText style={styles.title}>Ada yang bisa kami bantu?</CText>
          <TouchableOpacity style={styles.button}>
            <CText style={styles.textBtn}>Mulai Chat</CText>
          </TouchableOpacity>
        </View>

        <View style={styles.box}>
          <View
            style={{...styles.rowHeader, ...{justifyContent: 'space-between'}}}>
            <CText style={styles.title}>FAQ</CText>
            <CText>Lihat Semua</CText>
          </View>
          <View style={{marginVertical: 10}}>
            {faq_list.map((v, i) => {
              return (
                <View key={i} style={{borderBottomWidth: 0.3, padding: 15}}>
                  <CText style={styles.text}>{v.title}</CText>
                </View>
              );
            })}
          </View>
        </View>
        {/* Body End */}
      </Layout>
    );
  }
}

const styles = StyleSheet.create({
  box: {
    margin: 20,
    padding: 15,
    backgroundColor: colors.white,
    borderRadius: 10,
    elevation: 5,
  },
  rowHeader: {
    flexDirection: 'row',
    alignItems: 'center',
    margin: 5,
  },
  button: {
    padding: 10,
    backgroundColor: colors.primary,
    maxWidth: 150,
    borderRadius: 10,
    alignItems: 'center',
    marginVertical: 20,
  },
  title: {
    fontWeight: 'bold',
    fontSize: 16,
  },
  text: {
    fontWeight: 'bold',
  },
  textBtn: {
    color: colors.secondary,
    fontWeight: 'bold',
  },
});
