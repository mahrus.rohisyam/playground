import * as React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import {Home, Account, Discover, Chat, Splash, Playground} from '../screens';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import BottomTabNavigator from './TabNav/BottomTabNavigator'

const Stack = createNativeStackNavigator();
const Tab = createBottomTabNavigator();
const Hide = {headerShown: false};

const TabNav = () => {
  return (
    <Tab.Navigator tabBar={props => <BottomTabNavigator {...props} />}>
      <Tab.Screen options={Hide} name="Home" component={Home} />
      <Tab.Screen options={Hide} name="Discover" component={Discover} />
      <Tab.Screen options={Hide} name="Chat" component={Chat} />
      <Tab.Screen options={Hide} name="Account" component={Account} />
    </Tab.Navigator>
  );
};

const App = () => {
  return (
    <NavigationContainer>
      <Stack.Navigator initialRouteName="Splash">
        <Stack.Screen options={Hide} name="MainApp" component={TabNav} />
        <Stack.Screen options={Hide} name="Home" component={Home} />
        <Stack.Screen options={Hide} name="Playground" component={Playground} />
        <Stack.Screen options={Hide} name="Account" component={Account} />
        <Stack.Screen options={Hide} name="Discover" component={Discover} />
        <Stack.Screen options={Hide} name="Chat" component={Chat} />
        <Stack.Screen options={Hide} name="Splash" component={Splash} />
      </Stack.Navigator>
    </NavigationContainer>
  );
};

export default App;
