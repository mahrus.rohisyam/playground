import React from 'react';
import {Image, StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import {
  Account_Icon,
  Account_Icon_Active,
  Chat_Icon,
  Chat_Icon_Active,
  colors,
  Home_Icon,
  Home_Icon_Active,
  SOS_Icon,
  SOS_Icon_Active,
} from '../../assets';
import { CText } from '../../components';
import MI from 'react-native-vector-icons/MaterialIcons'

const TabItem = ({isFocused, onPress, label, onLongPress, index}) => {
  const Icon = () => {
    if (label == 'Home') {
      return isFocused ? (
        <Image style={styles.icon} source={Home_Icon_Active} />
      ) : (
        <Image style={styles.icon} source={Home_Icon} />
      );
    }
    if (label == 'Discover') {
      return isFocused ? (
        <MI color={colors.primary} size={35} name='search' />
      ) : (
        <MI color={colors.black} size={35} name='search' />
      );
    }
    if (label == 'Chat') {
      return isFocused ? (
        <Image style={styles.icon} source={Chat_Icon_Active} />
      ) : (
        <Image style={styles.icon} source={Chat_Icon} />
      );
    }
    if (label == 'Account') {
      return isFocused ? (
        <Image style={styles.icon} source={Account_Icon_Active} />
      ) : (
        <Image style={styles.icon} source={Account_Icon} />
      );
    }
  };
  return (
    <TouchableOpacity
      key={index}
      accessibilityRole="button"
      accessibilityState={isFocused ? {selected: true} : {}}
      onPress={onPress}
      onLongPress={onLongPress}
      style={{
        ...styles.button,
        ...{backgroundColor: isFocused ? colors.secondary : null},
      }}>
      <Icon />
      <CText style={{color: isFocused ? colors.primary : colors.grey, fontSize:12}}>
        {label}
      </CText>
    </TouchableOpacity>
  );
};

export default TabItem;

const styles = StyleSheet.create({
  button: {
    paddingVertical: 10,
    paddingHorizontal: 20,
    borderRadius: 10,
    alignItems: 'center',
  },
  icon:{
    width:35,
    height:35
  }
});
