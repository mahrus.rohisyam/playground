import React from 'react';
import {Provider} from 'react-redux';
import { PersistGate } from 'redux-persist/integration/react';
import Router from './src/navigation';
import { Persistor, Store } from './src/redux/store';

const App = () => {
  return (
    <PersistGate persistor={Persistor}>
      <Provider store={Store}>
        <Router />
      </Provider>
    </PersistGate>
  );
};

export default App;
